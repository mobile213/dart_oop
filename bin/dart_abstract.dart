import 'package:dart_abstract/dart_abstract.dart' as dart_abstract;
import 'dart:io';

void main(List<String> arguments) {
  Square square = new Square(3);
  square.area();
  Circle circle = new Circle(4);
  circle.area();
  Rectangle rectangle = new Rectangle(4, 6);
  rectangle.area();
  Triangle triangle = new Triangle(7, 16);
  triangle.area();
}

abstract class Shape {
  late int height;
  late int width;
  late int side;
  late double r;
  void area();
}

class Square extends Shape {
  Square(int x) {
    side = x;
  }

  @override
  void area() {
    print("Area of Square is ${side * side}");
  }
}

class Circle extends Shape {
  Circle(double x) {
    r = x;
  }

  @override
  void area() {
    print("Area of Circle is ${3.14 * (r * r)}");
  }
}

class Rectangle extends Shape {
  Rectangle(int x, int y) {
    height = x;
    width = y;
  }

  @override
  void area() {
    print("Area of Rectangle is ${height * width}");
  }
}

class Triangle extends Shape {
  Triangle(int x, int y) {
    height = x;
    width = y;
  }
  @override
  void area() {
    print("Area of triangle is ${height * width * 0.5}");
  }
}
